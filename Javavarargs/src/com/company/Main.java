package com.company;

public class Main {

    public static long wywolajMetodeConst1(Long n){
        long t = System.currentTimeMillis();
        for (int i = 0; i < n; i++) {
            bar("");
        }
        long wynik = System.currentTimeMillis() - t;
        System.err.println("Stala ilosc parametrow: " + wynik);

        return wynik;
    }

    public static long wywolajMetodeConst2(Long n){
        long t = System.currentTimeMillis();
        for (int i = 0; i < n; i++) {
            bar("","");
        }
        long wynik = System.currentTimeMillis() - t;
        System.err.println("Stala ilosc parametrow: " + wynik);

        return wynik;
    }

    public static long wywolajMetodeConst3(Long n){
        long t = System.currentTimeMillis();
        for (int i = 0; i < n; i++) {
            bar("","","");
        }
        long wynik = System.currentTimeMillis() - t;
        System.err.println("Stala ilosc parametrow: " + wynik);

        return wynik;
    }

    public static long wywolajMetodeConst4(Long n){
        long t = System.currentTimeMillis();
        for (int i = 0; i < n; i++) {
            bar("","","","");
        }
        long wynik = System.currentTimeMillis() - t;
        System.err.println("Stala ilosc parametrow: " + wynik);

        return wynik;
    }

    public static long wywolajMetodeConst5(Long n){
        long t = System.currentTimeMillis();
        for (int i = 0; i < n; i++) {
            bar("","","","","");
        }
        long wynik = System.currentTimeMillis() - t;
        System.err.println("Stala ilosc parametrow: " + wynik);

        return wynik;
    }

    public static long wywolajMetodeConst6(Long n){
        long t = System.currentTimeMillis();
        for (int i = 0; i < n; i++) {
            bar("","","","","", "");
        }
        long wynik = System.currentTimeMillis() - t;
        System.err.println("Stala ilosc parametrow: " + wynik);

        return wynik;
    }

    public static long wywolajMetodeConst7(Long n){
        long t = System.currentTimeMillis();
        for (int i = 0; i < n; i++) {
            bar("","","","","", "", "");
        }
        long wynik = System.currentTimeMillis() - t;
        System.err.println("Stala ilosc parametrow: " + wynik);

        return wynik;
    }

    public static long wywolajMetodeConst8(Long n){
        long t = System.currentTimeMillis();
        for (int i = 0; i < n; i++) {
            bar("","","","","", "", "", "");
        }
        long wynik = System.currentTimeMillis() - t;
        System.err.println("Stala ilosc parametrow: " + wynik);

        return wynik;
    }

    public static long wywolajMetodeConst9(Long n){
        long t = System.currentTimeMillis();
        for (int i = 0; i < n; i++) {
            bar("","","","","", "", "", "", "");
        }
        long wynik = System.currentTimeMillis() - t;
        System.err.println("Stala ilosc parametrow: " + wynik);

        return wynik;
    }

    public static long wywolajMetodeConst10(Long n){
        long t = System.currentTimeMillis();
        for (int i = 0; i < n; i++) {
            bar("","","","","","","","","","");
        }
        long wynik = System.currentTimeMillis() - t;
        System.err.println("Stala ilosc parametrow: " + wynik);

        return wynik;
    }

    public static long wywolajMetodeVarargs1(Long n){
        long t = System.currentTimeMillis();
        for (int i = 0; i < n; i++) {
            baz("");
        }
        long wynik = System.currentTimeMillis() - t;
        System.err.println("Varargs: " + wynik);

        return wynik;
    }

    public static long wywolajMetodeVarargs2(Long n){
        long t = System.currentTimeMillis();
        for (int i = 0; i < n; i++) {
            baz("", "");
        }
        long wynik = System.currentTimeMillis() - t;
        System.err.println("Varargs: " + wynik);

        return wynik;
    }

    public static long wywolajMetodeVarargs3(Long n){
        long t = System.currentTimeMillis();
        for (int i = 0; i < n; i++) {
            baz("", "" ,"");
        }
        long wynik = System.currentTimeMillis() - t;
        System.err.println("Varargs: " + wynik);

        return wynik;
    }

    public static long wywolajMetodeVarargs4(Long n){
        long t = System.currentTimeMillis();
        for (int i = 0; i < n; i++) {
            baz("", "" ,"", "");
        }
        long wynik = System.currentTimeMillis() - t;
        System.err.println("Varargs: " + wynik);

        return wynik;
    }

    public static long wywolajMetodeVarargs5(Long n){
        long t = System.currentTimeMillis();
        for (int i = 0; i < n; i++) {
            baz("", "" ,"", "" ,"");
        }
        long wynik = System.currentTimeMillis() - t;
        System.err.println("Varargs: " + wynik);

        return wynik;
    }

    public static long wywolajMetodeVarargs6(Long n){
        long t = System.currentTimeMillis();
        for (int i = 0; i < n; i++) {
            baz("", "" ,"", "" ,"","");
        }
        long wynik = System.currentTimeMillis() - t;
        System.err.println("Varargs: " + wynik);

        return wynik;
    }

    public static long wywolajMetodeVarargs7(Long n){
        long t = System.currentTimeMillis();
        for (int i = 0; i < n; i++) {
            baz("", "" ,"", "" ,"","", "");
        }
        long wynik = System.currentTimeMillis() - t;
        System.err.println("Varargs: " + wynik);

        return wynik;
    }

    public static long wywolajMetodeVarargs8(Long n){
        long t = System.currentTimeMillis();
        for (int i = 0; i < n; i++) {
            baz("", "" ,"", "" ,"","","","");
        }
        long wynik = System.currentTimeMillis() - t;
        System.err.println("Varargs: " + wynik);

        return wynik;
    }

    public static long wywolajMetodeVarargs9(Long n){
        long t = System.currentTimeMillis();
        for (int i = 0; i < n; i++) {
            baz("", "" ,"", "" ,"", "", "", "", "");
        }
        long wynik = System.currentTimeMillis() - t;
        System.err.println("Varargs: " + wynik);

        return wynik;
    }
    public static long wywolajMetodeVarargs10(Long n){
        long t = System.currentTimeMillis();
        for (int i = 0; i < n; i++) {
            baz("", "" ,"", "" ,"", "", "", "", "", "");
        }
        long wynik = System.currentTimeMillis() - t;
        System.err.println("Varargs: " + wynik);

        return wynik;
    }

    private static void baz(String... args){

    }

    private static void bar(String a1) {
    }

    private static void bar(String a1, String a2) {
    }

    private static void bar(String a1, String a2, String a3) {
    }

    private static void bar(String a1, String a2, String a3, String a4) {
    }

    private static void bar(String a1, String a2, String a3, String a4, String a5) {
    }

    private static void bar(String a1, String a2, String a3, String a4, String a5, String a6) {
    }

    private static void bar(String a1, String a2, String a3, String a4, String a5, String a6, String a7) {
    }
    private static void bar(String a1, String a2, String a3, String a4, String a5, String a6, String a7, String a8) {
    }
    private static void bar(String a1, String a2, String a3, String a4, String a5, String a6, String a7, String a8, String a9) {
    }
    private static void bar(String a1, String a2, String a3, String a4, String a5, String a6, String a7, String a8, String a9, String a10) {
    }

    public static void main(String[] args) {

        wywolajMetodeConst1(500000000L);
        wywolajMetodeConst2(500000000L);
        wywolajMetodeConst3(500000000L);
        wywolajMetodeConst4(500000000L);
        wywolajMetodeConst5(500000000L);
        wywolajMetodeConst6(500000000L);
        wywolajMetodeConst7(500000000L);
        wywolajMetodeConst8(500000000L);
        wywolajMetodeConst9(500000000L);
        wywolajMetodeConst10(500000000L);

        wywolajMetodeVarargs1(500000000L);
        wywolajMetodeVarargs2(500000000L);
        wywolajMetodeVarargs3(500000000L);
        wywolajMetodeVarargs4(500000000L);
        wywolajMetodeVarargs5(500000000L);
        wywolajMetodeVarargs6(500000000L);
        wywolajMetodeVarargs7(500000000L);
        wywolajMetodeVarargs8(500000000L);
        wywolajMetodeVarargs9(500000000L);
        wywolajMetodeVarargs10(500000000L);
    }
}
